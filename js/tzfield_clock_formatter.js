(function ($) {
  Drupal.behaviors.tzfieldClockFormatter = {
    attach: function (context, settings) {
      $('.tzfield-analogclock').each(function() {
        var $analogclock = $(this);
        var currentTimezoneOffset = (new Date()).getTimezoneOffset() * 60;
        var tzfieldTimezoneOffset = parseInt($analogclock.data('offset'));

        $analogclock.AnalogClock({
          skinsPath: Drupal.settings.basePath + Drupal.settings.tzfieldClockFormatterPath + '/js/analogclock/skins/',
          timeShift: (currentTimezoneOffset + tzfieldTimezoneOffset) * 1000
        })
      });
    }
  };
})(jQuery);
